﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UINetworkConnect : MonoBehaviour {
	[SerializeField] NetworkManager networkManager;
	[SerializeField] InputField ipInput;
	[SerializeField] Button buttonHost;
	[SerializeField] Button buttonJoin;

	void Awake () {
		buttonHost.onClick.AddListener (Host); //Subscribe to button click
		buttonJoin.onClick.AddListener (Join); //Subscribe to button click
	}

	void Host () {
		networkManager.StartHost(); //Start client host
	}

	void Join () {
		networkManager.networkAddress = ipInput.text; //Set IP address
		networkManager.StartClient(); //Connect to client
	}
}