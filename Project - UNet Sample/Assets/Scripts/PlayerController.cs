﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour { //Must use 
	/* 
		Move
		Aim
		Shoot
	*/

	[Header ("Shooting")]
	[SerializeField] GameObject bulletPrefab;
	[SerializeField] Transform bulletSpawn;

	[Header ("Controller")]
	[SerializeField] Transform cameraTransform;
	[SerializeField] float moveSpeed = 10;
	[SerializeField] float lookSpeed = 10;

	PlayerControls playerControls;

	void OnEnable () {
		if (!isLocalPlayer) return; //Disable control on non-local player

		playerControls.Enable (); //Enable input map
	}

	void OnDisable () {
		if (!isLocalPlayer) return; //Disable control on non-local player

		playerControls.Disable (); //Disable input map
	}

	void Start () {
		//Must happen in Start and not Awake to allow network to start up
		if (!isLocalPlayer) { //Disable control on non-local player and destroy extra camera
			Destroy (cameraTransform.gameObject);
			return;
		}

		playerControls = new PlayerControls (); //Create new instance of the PlayerControls map
		playerControls.Enable (); //Enable the map to begin tracking Input

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		playerControls.Gameplay.Move.performed += ctx => Move (ctx.ReadValue<Vector2> ()); //Subscribe to event but specify how to handle the data passed with the event | Axis data in Vector 2 format
		playerControls.Gameplay.Look.performed += ctx => Look (ctx.ReadValue<Vector2> ()); //Subscribe to event but specify how to handle the data passed with the event | Axis data in Vector 2 format
		playerControls.Gameplay.Shoot.performed += ctx => Shoot (); //Subscribe to event but specify how to handle the data passed with the event | no data needed
	}

	void Move (Vector2 moveVector) {
		if (!Application.isFocused) return;

		moveVector *= Time.deltaTime * moveSpeed;

		if (moveVector.x != 0 || moveVector.y != 0) { //No need to call the event if there is no input
			//Move
			transform.Translate (new Vector3 (moveVector.x, 0, moveVector.y), Space.Self);
		}
	}

	void Look (Vector2 lookVector) {
		if (!Application.isFocused) return;

		lookVector *= Time.deltaTime * lookSpeed;

		//Look
		transform.Rotate (new Vector3 (0, lookVector.x, 0), Space.Self);
		cameraTransform.Rotate (new Vector3 (-lookVector.y, 0, 0), Space.Self);
	}

	void Shoot () {
		if (!Application.isFocused) return;

		CmdSpawnBullet ();
	}

	[Command] //Use the Command attribute to indicate that this function runs on the server instance of this gameobject
	void CmdSpawnBullet () { //Needs to have the "Cmd" prefix to work
		GameObject newBullet = Instantiate (bulletPrefab);
		NetworkServer.Spawn (newBullet); //Spawn the prefab on the server. Authentication on server to prevent client side hacks.
		newBullet.transform.position = bulletSpawn.position; //preset variables will persist since they're part of the prefab
		newBullet.GetComponent<Rigidbody> ().AddForce (bulletSpawn.transform.forward * 50, ForceMode.Impulse);
	}

}